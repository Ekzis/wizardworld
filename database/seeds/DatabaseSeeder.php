<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

		User::create(array(
            'email' => 'foo@bar.com',
            'password' => '123123',
            'first_name' => 'qqq',
            'last_name' => 'www',
            'telephone' => '44444',
            'passport_series' => '431',
            'passport_number' => '42121',
        ));
        $this->command->info('Таблица пользователей заполнена данными!');
    }
}

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		User::create(array(
            'email' => 'foo@bar.com',
            'password' => 'foo@bar.com',
            'first_name' => 'foo@bar.com',
            'last_name' => 'foo@bar.com',
            'telephone' => 'foo@bar.com',
            'passport_series' => 'foo@bar.com',
            'passport_number' => 'foo@bar.com',
        ));
	}

}
