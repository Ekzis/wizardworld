@extends('layouts.app')
@section('title', 'Welcome')

@section('link')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@endsection

@section('content')
<div class="diary">

    @include('inc.messages')
    
    <div class="row">
        @guest
        @else
            @if (Auth::user() && Auth::user()->permission == 'admin')
                <div class="col-lg-3 col-md-4 col-6 thumb addImg">
                    <i class="fa fa-plus" data-toggle="modal" data-target="#exampleModal"></i>
                </div>
            @endif
        @endguest
        

        @foreach ($urls as $item)
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="{{ Storage::url($item['url']) }}"> 
                <img class="img-fluid" src="{{ Storage::url($item['url']) }}" alt="..."> 
            </a>
            @if (Auth::user() && Auth::user()->permission == 'admin')
                <form method="post" id="delete-img" action="{{ route('diary-img-delete') }}">
                    @csrf
                    <div class="block_delete_img">
                        <i class="fa fa-trash text-danger" name="id" value="{{$item['id']}}"></i>
                        <input type="hidden" name="id" value="{{$item['id']}}">
                    </div>
                </form>
            @endif
        </div>
        @endforeach
        
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">{{__('gallery.modal_title_text')}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="addImgForm" method="post" action="{{ route('diary-img-upload') }}" enctype="multipart/form-data">
                @csrf
                <input type="file" name="image">
                <div class="text text-danger">
                    <p class="form-label">{{ __('gallery.modal_permission_text') }} .zip</p>
                    <p class="form-label">{{ __('gallery.modal_permission_in_file_text') }} .jpg/.jpeg</p>
                </div>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('content.close_text') }}</button>
          <button type="submit" form="addImgForm" class="btn btn-primary">{{ __('content.add_text') }}</button>
        </div>
      </div>
    </div>
</div>

<script>
    $('.diary').on('click', '.block_delete_img i', function() {
        $('#delete-img').submit();
    });
</script>
@endsection