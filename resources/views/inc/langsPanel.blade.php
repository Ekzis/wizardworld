@if (isset($langs))
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{__('header.langs_text')}} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            @foreach ($langs as $lang)
                <a class="dropdown-item" href="{{ route('set-lang', $lang) }}">{{ $lang }}</a>
            @endforeach
        </div>
    </li>
@endif
