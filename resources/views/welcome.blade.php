@extends('layouts.app')
@section('title', 'Welcome')

@section('content')

<div class="grid-container">
    <div class="welcome">
        <div class="jumbotron">
            <div class="container">
              <h1 class="display-3">{{ __('welcome.download_title_text') }}</h1>
              <p>{{ __('welcome.download_text') }}</p>
              <p><a class="btn btn-primary btn-lg" href="{{ $url }}" role="button">{{ __('welcome.download_btn_text') }}</a></p>
            </div>
        </div>
    </div>
    <div class="diary">
        <div class="jumbotron">
            <div class="container">
              <h1 class="display-3">{{ __('welcome.gallery_title_text') }}</h1>
              <p>{{ __('welcome.gallery_text') }}</p>
              <p><a class="btn btn-primary btn-lg" href="{{ route('diary') }}" role="button">{{ __('welcome.gallery_btn_text') }}</a></p>
            </div>
        </div>
    </div>
    <div class="forum">
        <div class="jumbotron">
            <div class="container">
              <h1 class="display-3">{{ __('welcome.forum_title_text') }}</h1>
              <p>{{ __('welcome.forum_text') }}</p>
              <p><a class="btn btn-primary btn-lg" href="{{ route('forum') }}" role="button">{{ __('welcome.forum_btn_text') }}</a></p>
            </div>
        </div>
    </div>
  </div>

@endsection
