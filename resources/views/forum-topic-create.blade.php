@extends('layouts.app')
@section('title', 'Create topic')

@section('content')
<div class="topic">

  <div class="my-3 p-3 bg-white rounded box-shadow">
      <div class="topics-title border-bottom border-gray pb-2 mb-0">
          <h6 class="">Create topic</h6>
          <a href="{{ route('forum') }}" type="button" class="btn btn-secondary back">Back</a>

      </div>
    
      <form action="{{ route('forum-topic-creating') }}" method="POST">
        @csrf
        <div class="row">
          <div class="col-12">
            <label for="title" class="form-label">{{ __('content.title_text') }}</label>
            <input name="title" type="text" class="form-control" id="title" placeholder="{{ __('content.title_text') }}">
          </div>
          
          <div class="col-12">
            <label for="about" class="form-label">{{ __('content.about_text') }}</label>
            <input name="about" type="text" class="form-control" id="about" placeholder="{{ __('content.about_text') }}">
          </div>

          <div class="col-12">
            <label for="text" class="form-label">{{ __('content.text') }}</label>
            <input name="text" type="textarea" class="form-control" id="text" placeholder="{{ __('content.text') }}">
          </div>
          {{-- <input type="number" class="d-none" value=""> --}}
        </div>
        <button type="submit" class="btn btn-success mt-3">{{ __('content.create_text') }}</button>
      </form>
      
  </div>
  
</div>
@endsection