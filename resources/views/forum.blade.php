@extends('layouts.app')
@section('title', 'Welcome')

@section('content')
    <div class="topic">
        <div class="my-3 p-3 bg-white rounded box-shadow">
            <div class="topics-title border-bottom border-gray pb-2 mb-0">
                <div class="d4">
                    <form>
                        <input type="text" class="inputSearch" placeholder="{{ __('forum.placeholder_search_text') }}">
                        <button class="btnSearch" type="button"></button>
                    </form>
                </div>
            </div>
            <ul class="topic_block">
                <div class="topic_block-items">
                    @foreach ($topics as $item)
                        <li class="row topic_block-item">
                            <a href="{{ route('topic', $item['topic_id']) }}" class="media pt-3 topic-item @if (Auth::user() && Auth::user()->permission == 'admin') col-10
                            col-md-11 @else col-12 @endif">
                                <img data-src="holder.js/32x32?theme=thumb&amp;bg=007bff&amp;fg=007bff&amp;size=1"
                                    alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;"
                                    src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1795ad3cb70%20text%20%7B%20fill%3A%23007bff%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1795ad3cb70%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23007bff%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.828125%22%20y%3D%2216.965625%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                    data-holder-rendered="true">
                                <div class="text-muted media-body pb-3 mb-0 small lh-125 h-25">
                                    <p class="d-block text-gray-dark">{{ __('content.name_text') }}:
                                        {{ $item['first_name'] . ' ' . $item['last_name'] }}</p>
                                    <p class="d-block text-gray-dark">{{ __('content.title_text') }}:
                                        {{ $item['title'] }}</p>
                                    <p class="text">
                                        {{ $item['text'] }}
                                    </p>
                                </div>
                            </a>
                            @guest
                            @else
                                @if (Auth::user() && Auth::user()->permission == 'admin')
                                    <button type="button" class="btn btn-danger btnDelete col-2 col-md-1"
                                        value="{{ $item['topic_id'] }}"><i class="fa fa-trash"></i></button>
                                @endif
                            @endguest
                        </li>
                    @endforeach
                </div>
                <div class="topic_block-pagination">
                    {{ $topics->links() }}
                </div>
            </ul>
            <div class="row create-topic">
                @guest
                @else
                    <div class="col-md-8">
                        <a href="{{ route('forum-topic-create') }}" class="btn btn-primary">
                            {{ __('forum.create_topic_text') }}
                        </a>
                    </div>
                @endguest
                <div class="col-md-4">
                    <small class="d-block text-right mt-3">
                        <a href="{{ route('forum') }}">{{ __('forum.all_topics_text') }}</a>
                    </small>
                </div>
            </div>
        </div>

    </div>
    <script>
        $('.inputSearch').on('input', function() {
            var text = $('.inputSearch').val();
            $.ajax({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                url: '{{ route('forum-search') }}',
                method: 'post',
                dataType: 'html',
                data: {
                    title: text
                },
                beforeSend: function() {
                    // $('.topic_block').slideUp().html();
                    $('.topic_block').html();

                },
                success: function(data) {
                    $('.topic_block').html(data);

                    // alert(data);
                    // console.log(data);
                }
            });
        });

        $('.topic_block').on('click', '.btnDelete', function() {
            var topic_id = $(this).val();
            _this = $(this);
            $.ajax({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                url: '{{ route('forum-topic-delete') }}',
                method: 'post',
                dataType: 'html',
                data: {
                    topic_id: topic_id
                },
                success: function(data) {
                    _this.parents('li').remove();
                }
            });
        });

    </script>
@endsection
