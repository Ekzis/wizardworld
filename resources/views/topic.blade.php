@extends('layouts.app')
@section('title', 'Welcome')

@section('content')
<div class="topic">
  <div class="my-3 p-3 bg-white rounded box-shadow">
      <div class="topic-title border-bottom border-gray pb-2 mb-0">
          <h4 class="">{{ $data[0]->title }}</h4>
          <a href="{{ route('forum') }}" type="button" class="btn btn-secondary back">{{ __('content.back_text') }}</a>
      </div>
      <div class="grid-container">
        <div class="user">
            <p>{{ __('forum.first_name_text') }}: {{ $data[0]->first_name }}</p>
            <p>{{ __('forum.last_name_text') }}: {{ $data[0]->last_name }} </p>
        </div>
        <div class="comment">
            {{ $data[0]->text }}
        </div>
      </div>
      

      @foreach ($data['comments'] as $item)
        <div class="grid-container">
            <div class="user">
                <p>{{ __('forum.first_name_text') }}: {{ $item->first_name }}</p>
                <p>{{ __('forum.last_name_text') }}: {{ $item->last_name }} </p>
                @guest

                @else
                    @if ($item->customer_id != Auth::user()->customer_id)
                    <button type="button" class="btn btn-secondary to-answer" value="{{ $item->id }}">{{ __('forum.to_answer_text') }}</button>
                    @endif
                  
                @if (Auth::user()->permission == 'admin')
                    <button type="button" class="btn btn-danger btnDelete" value="{{ $item->id }}"><i class="fa fa-trash"></i></button>
                @endif
         
                @endguest

            </div>
            <div class="comment">
                @if ($item->re_text)
                    <div class="comment-re-blcok">
                        <div class="comment-re-blcok_name">
                            {{ $item->re_name }} {{ __('forum.answer_text') }}:
                        </div>
                        <div class="comment-re-blcok_text">
                            {{ $item->re_text }}
                        </div>
                    </div>
                @endif
                <div class="comment-text">
                    {{ $item->text }}
                </div>
            </div>
        </div>
      @endforeach


      
      @guest
      <small class="d-block text-right mt-3">
        <a href="#">{{ __('forum.all_topics_text') }}</a>
      </small>
      @else
            <div class="grid-container">
                <div class="user">
                    <p>{{ __('forum.first_name_text') }}: {{ Auth::user()->first_name }}</p>
                    <p>{{ __('forum.last_name_text') }}: {{ Auth::user()->last_name }} </p>
                    <button form="set-comment" type="submit" class="btn btn-success">{{ __('content.send_text') }}</button>
                </div>
                <form id="set-comment" action="{{ route('createComment', $data[0]->topic_id) }}" method="POST">
                    @csrf
                    <div class="comment">
                        <textarea class="text-send" name="text" rows="8"></textarea>
                    </div>
                </form>
            </div>
       @endguest
  </div>
</div>
<script>
    $('.to-answer').on('click', function () {
        var answer_id = $(this).val();
        removeAnswer();
        $('.text-send').focus();
        $('#set-comment').append("<input type='hidden' id='answer_id' name='answer_id' value=" + answer_id + ">");
        $('#set-comment .comment').append("<span class='answer_to'>{{ __('forum.dont_send_text')  }}  <i class='fa fa-times'></i></span>"); 
    });

    $('.comment').on('click', '.answer_to', function () {
        $('.text-send').focus();
        removeAnswer();
    });

    function removeAnswer() {
        $('#answer_id').remove();
        $('.answer_to').remove();
    }

    $('.user').on('click', '.btnDelete', function () {
        var comment_id = $(this).val();
        _this = $(this);
        $.ajax({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            url: '{{ route("forum-topic-comment-delete") }}' ,
            method: 'post',
            dataType: 'html',
            data: {
                comment_id: comment_id
            },
            success: function(data) {
                _this.parents('.grid-container').remove();
            }
        });
    });
</script>
@endsection