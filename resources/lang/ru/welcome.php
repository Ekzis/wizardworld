<?php

return [

    'gallery_title_text' => 'Галерея',
    'gallery_text' => 'Скриншоты процесса игры',
    'gallery_btn_text' => 'Посмотреть',

    'download_title_text' => 'Играть',
    'download_text' => 'Тут вы можете скачать игру',
    'download_btn_text' => 'Скачать',

    'forum_title_text' => 'Форум',
    'forum_text' => 'Тут вы можете пообщаться с другими игроками',
    'forum_btn_text' => 'Войти',

];