<?php

return [

    'placeholder_search_text' => 'Искать здесь...',
    'create_topic_text' => 'Создать тему',
    'all_topics_text' => 'Все темы',
    'first_name_text' => 'Имя',
    'last_name_text' => 'Фамилия',
    'dont_send_text' => 'Не отвечать на сообщение',
    'to_answer_text' => 'Ответить',
    'answer_text' => 'Сказал',
    

];