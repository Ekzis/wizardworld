<?php

return [

    'close_text' => 'Закрыть',
    'add_text'   => 'Добавить',
    'name_text'  => 'Имя',
    'title_text' => 'Тема',
    'about_text'    => 'О чем?',
    'send_text'  => 'Отправить',
    'back_text'  => 'Назад',
    'text'          => 'Текст',
    'create_text'   => 'Создать',

];