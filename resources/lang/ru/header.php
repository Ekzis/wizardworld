<?php

return [

    'about_text'     => 'О нас',
    'login_text'     => 'Войти',
    'register_text'  => 'Зарегестрироваться',
    'langs_text'     => 'Язык',
    'logout_text'    => 'Выйти',
    'throttle'       => 'Слишком много попыток входа в систему. Пожалуйста, повторите попытку через :секунды секунды.',

];