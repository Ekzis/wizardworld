<?php

return [

    /* text */
    'modal_title_text'              => 'Добавить',
    'modal_permission_text'         => 'Допустимое разрешение:',
    'modal_permission_in_file_text' => 'Допустимое разрешение файлов в архиве:',

    /* errors */
    'invalid_permission_error'      => 'У файла :file недопустимое разрешение',
    'already_exists_error'          => 'Файл :file уже существует',
    'repeat_error'                  => 'Упс. Попробуйте ещё раз',
    'zip_invalid_permission_error'  => 'Файл должен быть формата .zip',

    /* success */
    'file_add_success'              => 'Файл :file был добавлен',

];