<?php

return [

    'gallery_title_text'    => 'Gallery',
    'gallery_text'          => 'Screenshots of the game process',
    'gallery_btn_text'      => 'View',

    'download_title_text'   => 'Play',
    'download_text'         => 'Here you can download the game',
    'download_btn_text'  => 'Download',

    'forum_title_text'      => 'Forum',
    'forum_text'            => 'Here you can chat with other players',
    'forum_btn_text'        => 'Log in',

];