<?php

return [

    'placeholder_search_text' => 'Search here...',
    'create_topic_text'       => 'Create a topic',
    'all_topics_text'         => 'All topics',
    'first_name_text'         => 'Name',
    'last_name_text'          => 'Last name',
    'dont_send_text'          => "Don't reply to message",
    'to_answer_text'          => 'Reply',
    'answer_text'             => 'Said',
    

];