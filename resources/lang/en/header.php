<?php

return [

    'about_text'    => 'About us',
    'login_text'    => 'Log in',
    'register_text' => 'Register',
    'langs_text'    => 'Language',
    'logout_text'   => 'Exit',
    'throttle'      => 'Too many login attempts. Please try again in :seconds seconds.',

];