<?php

return [

    'close_text'    => 'Close',
    'add_text'      => 'Add',
    'name_text'     => 'Name',
    'title_text'    => 'Title',
    'about_text'    => 'About what?',
    'send_text'     => 'Send',
    'back_text'     => 'Go back',
    'text'          => 'Text',
    'create_text'   => 'Create',   

];