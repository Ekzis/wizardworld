<?php

return [

    /* text */
    'modal_title_text'              => 'Add',
    'modal_permission_text'         => 'Acceptable resolution:',
    'modal_permission_in_file_text' => 'Allowed file resolution in the archive:',

    /* errors */
    'invalid_permission_error'      => 'File: file invalid permission',
    'already_exists_error'          => 'File :file already exists',
    'repeat_error'                  => 'Oops. Try again',
    'zip_invalid_permission_error'  => ' The file must be of the following format .zip',

    /* success */
    'file_add_success'              => 'File :file was added',

];