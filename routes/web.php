<?php

use App\Http\Controllers\DiaryController;

Route::get('/login', 'HomeController@index')->name('login');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/forum', 'ForumController@index')->name('forum');
Route::get('/forum/topic/{topic_id}', 'ForumController@getTopic')->name('topic');
Route::post('/forum/topic/{topic_id}/createComment', 'ForumController@setComment')->name('createComment');
Route::post('/forum/create', 'ForumController@createTopic')->name('forum-topic-creating');
Route::post('/forum', 'ForumController@searchTopics')->name('forum-search');
Route::post('/forum/topic/topicDelete', 'ForumController@deleteTopic')->name('forum-topic-delete');
Route::post('/forum/topic/commentDelete', 'ForumController@deleteComment')->name('forum-topic-comment-delete');
// Route::resource('/forum/search', 'ForumController@searchTopics')->name('forum-search');

Route::get('/forum/create', function () {
    return view('forum-topic-create');
})->name('forum-topic-create');

Route::get('/diary', 'DiaryController@index')->name('diary');
Route::post('/diary/upload', 'DiaryController@fileUpload')->name('diary-img-upload');
Route::post('/diary/delete', 'DiaryController@deleteImg')->name('diary-img-delete');

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::get('/contact', function () {
    return view('contact');
});

Route::get('setlocale/{locale}', function ($locale) {
    
    if (in_array($locale, Config::get('app.locales'))) {   # Проверяем, что у пользователя выбран доступный язык 
    	Session::put('locale', $locale);                    # И устанавливаем его в сессии под именем locale
    }

    return redirect()->back();                              # Редиректим его <s>взад</s> на ту же страницу

})->name('set-lang');


Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');
