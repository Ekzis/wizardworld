<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Models\DiaryModel;
use Error;
use Illuminate\Support\Facades\Auth;
use ZipArchive;

class DiaryController extends Controller
{
    public function index()
    {
        // $data['url'] = Storage::url('images/img.jpg');
        $data['urls'] = $this->getAllData();
        $data['langs'] = get_lang();

        return view('diary', $data);
    }

    public function addImg()
    {
        // DiaryModel::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle'])
    }
    public function fileUpload(Request $request){
        $data['urls'][] = $this->getAllData();

        if($request->isMethod('post')){
    
            if($request->hasFile('image')) {
                // dd($request);
                $zip = new ZipArchive;
                $file = $request->file('image');
                $fileName = $file->getClientOriginalName(); 

                if (!preg_match('#\.(zip)$#i', $fileName)) {
                    $error[] = __('gallery.zip_invalid_permission_error');
                    return redirect()->route('diary', $data)->withErrors($error);
                }

                $file->move(storage_path() . '\app\public\images', $fileName);

                $res = $zip->open(storage_path() . '\app\public\images\\' . $fileName);

                if ($res === true) {

                    $zip->extractTo(storage_path() . '\app\public\images');
                    $count = $zip->numFiles;

                    for ($i = 0; $i < $count; $i++) { 
                        $stat = $zip->statIndex($i);
                        if (preg_match('#\.(jpg|jpeg)$#i', $stat['name'])){
                            $meta_info_about_file = pathinfo($stat['name']);
                            
                            $img_array[$i]['name'] = $meta_info_about_file['basename'];
                            $img_array[$i]['extension_type'] = $meta_info_about_file['extension'];            
                            
                            try {
                                DiaryModel::insert([
                                    ['url' => 'images/' . $stat['name']]
                                ]);
                                $success[] = __('gallery.file_add_success', ['file' => $stat['name']]);
                            } catch (QueryException $e) {
                                switch ($e->getCode()) {
                                    case '23000':
                                        $error[] = __('gallery.already_exists_error', ['file' => $stat['name']]);
                                    break;
                                    default:
                                        $error[] = __('gallery.repeat_error');
                                    break;
                                }
                            }

                        } else {
                            $error[] = __('gallery.invalid_permission_error', ['file' => $stat['name']]);
                        }
                    }

                    // dd(storage_path() . '\app\public\images\\' . $fileName);
                    // unlink(storage_path() . '\app\public\images\\' . $fileName);
                  
                }
            }
        }
        
        // dd($error, $success);
        if (isset($error) && isset($success)) {
            return redirect()->route('diary', $data)->with('success', $success)->withErrors($error);
            // return redirect()->route('diary', $data)->with('errors', $error);
        }
        if (isset($error)) {
            return redirect()->route('diary', $data)->withErrors($error);
            // return redirect()->route('diary', $data)->with('errors', $error);
        }
        if (isset($success)) {
            return redirect()->route('diary', $data)->with('success', $success);
        } 
        

        return redirect()->route('diary', $data);
     
    }
    public function getAllData()
    {
        return DiaryModel::all();
    }
    public function deleteImg(Request $req)
    {
        DiaryModel::where('id', '=', $req->id)->delete();
        $data['urls'] = $this->getAllData();
        return redirect()->route('diary', $data);
    }
}
