<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        $data['url'] = Storage::url('game.zip');
        $data['langs'] = get_lang();
        // dd(compact('data'), $data);

        return view('welcome', $data);
    }
}
