<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\ForumModel;
use  App\Models\TopicModel;
use Illuminate\Support\Facades\Auth;

class ForumController extends Controller
{
    public function index()
    {
        
        $data['topics'] = $this->getAllData();
        $data['langs'] = get_lang();
        // dd(compact('data'), $data);

        return view('forum', $data);
    }

    public function createTopic(Request $request)
    {
        // dd($request->session());
        $forInsert = $_POST;
        $forInsert['customer_id'] = Auth::user()->customer_id;
        $this->setTopic($forInsert);

        $data['topics'] = $this->getAllData();

        return redirect()->route('forum', compact('data'));

    }
    public function getAllData()
    {
        // return ForumModel::leftjoin('users', 'forum.customer_id', '=', 'users.customer_id')->get();
        return ForumModel::leftjoin('users', 'forum.customer_id', '=', 'users.customer_id')->paginate(10);

    }

    public function searchTopics()
    {
        $title = trim($_POST['title']);
        // return $title;
        if (!empty($title)) {
            $data['topics'] = ForumModel::where('forum.title', 'LIKE', '%' . $title . '%')->orWhere('forum.about', 'LIKE', '%' . $title . '%')->paginate(NULL);
        } else {
            $data['topics'] = $this->getAllData();
        }

        return view('inc.topic_block', compact('data'));
    }

    public function setTopic($data)
    {
        ForumModel::insert(
            [
                'customer_id' => $data['customer_id'],
                'title' => $data['title'],
                'about' => $data['about'],
                'text' => $data['text'],
            ]
        );
    }

    public function getTopic($topic_id)
    {
        // $data = ForumModel::leftjoin('forum_topic', 'forum.customer_id', '=', 'forum_topic.customer_id')->where('forum.topic_id', '=', $topic_id)->get();
        $data = ForumModel::leftjoin('users', 'forum.customer_id', '=', 'users.customer_id')->where('forum.topic_id', '=', $topic_id)->get();
        $data['comments'] = $this->getComments($topic_id);
        // dd($data);
        return view('topic', compact('data'));
    }
    public function deleteComment()
    {
        $id = $_POST['comment_id'];
        // return ForumModel::leftjoin('users', 'forum.customer_id', '=', 'users.customer_id')->get();
        TopicModel::where('id', '=', $id)->delete();

    }
    public function deleteTopic()
    {
        $id = $_POST['topic_id'];
        // return ForumModel::leftjoin('users', 'forum.customer_id', '=', 'users.customer_id')->get();
        TopicModel::where('topic_id', '=', $id)->delete();
        ForumModel::where('topic_id', '=', $id)->delete();

    }
    

    public function getComments($topic_id)
    {
        // $data = TopicModel::leftjoin('users', 'forum_topic.customer_id', '=', 'users.customer_id')->where('forum_topic.topic_id', '=', $topic_id)->get();
        $data = TopicModel::getTopic($topic_id);
        // dd($data);
        return $data;
    }

    public function setComment($topic_id)
    {
        $data = $_POST;
        $data['customer_id'] = Auth::user()->customer_id;

        TopicModel::insert(
            [
                'topic_id' => $topic_id,
                'customer_id' => $data['customer_id'],
                'answer_id' => isset($data['answer_id']) ? $data['answer_id'] : NULL,
                'text' => $data['text'],
            ]
        );
        return redirect()->route('topic', $topic_id);
    }

}
