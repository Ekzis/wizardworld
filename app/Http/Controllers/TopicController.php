<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TopicModel;

class TopicController extends Controller
{
    public function index($topic_id)
    {
        $topic = new TopicModel();
        $data = $topic->leftJoin('')->where('topic_id', '==', $topic_id);
        return view('topic', compact('data'));
    }
}
