<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Config;
use Session;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $raw_locale = Session::get('locale');     # Если пользователь уже был на нашем сайте, 

        if (in_array($raw_locale, Config::get('app.locales'))) {  # Проверяем, что у пользователя в сессии установлен доступный язык 
            $locale = $raw_locale;                                # (а не какая-нибудь бяка) 
        } else {
            $locale = Config::get('app.locale');
        }
        App::setLocale($locale);       # Устанавливаем локаль приложения
      

        return $next($request);
    }
}
