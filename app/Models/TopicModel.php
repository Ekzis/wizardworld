<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class TopicModel extends Model
{
    protected $table = 'forum_topic';

    // public static function getTopic($id) 
    // {
    //     return static::where('id', $id);
    // }
    public static function getTopic($id) 
    {
        return DB::select("SELECT
        ft.*, u.first_name, u.last_name,
        (SELECT CONCAT(first_name, ' ', last_name) FROM forum_topic ft2 LEFT JOIN users as u ON (ft2.customer_id = u.customer_id) where (ft.answer_id = ft2.id)) as re_name,
        (SELECT ft2.text FROM forum_topic ft2 LEFT JOIN users as u ON (ft2.customer_id = u.customer_id) where (ft.answer_id = ft2.id)) as re_text
        FROM `forum_topic` as ft LEFT JOIN users as u ON (ft.customer_id = u.customer_id) WHERE ft.topic_id = ?", [$id]);
    }
}
